Feature: Allow to save an user information
  To guarantee security 
  As a developer
  I want to create a service  that  create and  validate session  tokens 

  Background:
    Given I start the API with ISO configuration
    
  Scenario: Returns 404 when the api was asked with wrong endpoint 
    Given I set the correct request header
    When I call the following endpoint "http://localhost:3010/api/notFound"          
    Then I see the following result for the response
    | statusCode  | statusMessage  |
    | 404         | Not Found      |

  Scenario: Returns 401 when the api was asked with wrong header
    Given I set the wrong request header
    When I call the following endpoint "http://localhost:3010/api/v1/save-info"
    Then I see the following result for the response
    | statusCode  | statusMessage |
    | 401         | Unauthorized  |

  Scenario: Returns 200 when the api was asked with a valid header and valid data
    Given I set the following header
    | key            | value |
    |x-access-token|M2UzOTUxZjNkZWYwNGQzMzJiYWJlYTE4d|
    |Content-Type|application/json|
    
    Given I set the correct body
    When I call the following endpoint "http://localhost:3010/api/v1/save-info"
    Then I see the following result for the response 
    | statusCode  | statusMessage |
    | 200         | OK |
    
  #Scenario: Returns  an error when the rut parameter was passed badly 
  #  Given I set the rut parameter with a wrong rut
  #  |rut|
  #  |12.122.122-1|
  #  When I call the following endpoint "http://localhost:3004/api/generar_sesion/v1"
  #  Then I see the following result for the response
  #  | statusCode  | statusMessage  | message           |
  #  | 403         | Forbidden      | Invalid parameters|
  #  And I see the following message in the body response 
  #  | code  | message           |
  #  | error | Invalid parameters|

  #Scenario: Returns  an Internal Server Error 
    #Given I set the id with just an string
    #|id|
    #|ssasasasasasasasasasasa|
    #When I call the following endpoint "http://localhost:3004/api/solicitud_apertura/v1/generar_sesion/"
    #Then I see the following result for the response
    #| statusCode  | statusMessage  |
    #| 500         |Internal Server Error|
    
  #Scenario: Returns false when the token is not passed as a header parameter
  #  Given I set the rut parameter without the header x-access-token   
  #  |rut|
  #  |11.111.111-1|
  #  When I call the following endpoint "http://localhost:3004/api/validar_sesion/v1"
  #  Then I see the following result for the response
  #  | statusCode  | statusMessage  |
  #  | 200         | OK             | 
  #  And I see the following fields in the body response 
  #  | isValid  |
  #  | false    |

  #Scenario: Returns a token
  #  Given I set the rut parameter without the header x-access-token   
  #  |rut|
  #  |11.111.111-1|  
  #  When I call the following endpoint "http://localhost:3004/api/generar_sesion/v1/"
  #  Then I see the following result for the response
  #  | statusCode  | statusMessage  |
  #  | 200         | OK             | 
  #  And I see the following fields in the body response 
  #  | code |
  #  | 1    |

  #Scenario: Returns the same  token
  #  Given I set the rut parameter without the header x-access-token   
  #  |rut|
  #  |11.111.111-1|  
  #  When I call the following endpoint "http://localhost:3004/api/generar_sesion/v1"
  #  Then I see the following result for the response
  #  | statusCode  | statusMessage  |
  #  | 200         | OK             | 
  #  And I see the following fields in the body response 
  #  | code |
  #  | 1    |    
  #  Then I set the rut parameter with  the header x-access-token gotten form the before step
  #  And I call the following endpoint "http://localhost:3004/api/generar_sesion/v1"
  #  Then I see the following result for the response
  #  | statusCode  | statusMessage  |
  #  | 200         | OK             |
  #  And I see the following fields in the body response 
  #  | code |
  #  | 0    |    

  #Scenario: Returns true with a valid token
  #  Given I set the rut parameter without the header x-access-token   
  #  |rut|
  #  |11.111.111-1|  
  #  When I call the following endpoint "http://localhost:3004/api/generar_sesion/v1/"
  #  Then I see the following result for the response
  #  | statusCode  | statusMessage  |
  #  | 200         | OK             | 
  #  And I see the following fields in the body response 
  #  | code |
  #  | 1    |    
  #  Then I set the rut parameter with  the header x-access-token gotten form the before step
  #  And I call the following endpoint "http://localhost:3004/api/validar_sesion/v1/"
  #  Then I see the following result for the response
  #  | statusCode  | statusMessage  |
  #  | 200         | OK             |
  #  And I see the following fields in the body response 
  #  | isValid  |
  #  | true    |
