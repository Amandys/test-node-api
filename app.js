global.Promise = require('bluebird');  

const express = require('express');
const morgan = require('morgan');
const FileStreamRotator = require('file-stream-rotator');
const logger = require('./app/loggers/logger');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const config = require('./app/config/index');
const compression = require('compression');
const securityMiddleware = require('./app/middlewares/security-middleware');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.options('*', (req, res) => {
  res.header('Access-Control-Allow-Origin', req.get('Origin') || '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Accept-Encoding');
  res.status(200).end();
});

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Accept-Encoding');
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');
  next();
});

const logOptions = () => {
  const logStream = FileStreamRotator.getStream({
    date_format: 'YYYY-MM-DD',
    filename: 'logs/access-%DATE%.log',
    frequency: 'daily',
    verbose: false,
  });
  return { stream: logStream };
};

const logFormat = process.env.LOG_FORMAT || 'dev';
app.use(morgan(logFormat, logOptions()));

const BASE_PATH = config.basePath;
const API_V1 = config.versionNumber;

app.use(compression());

app.get(`${BASE_PATH}${API_V1}/health`, (req, res) => {
  res.send('up and running');
});

app.use(securityMiddleware);

app.use(`${BASE_PATH}${API_V1}/`, require('./app/routes/save-info-post-route'));

app.use(`${BASE_PATH}${API_V1}/`, require('./app/routes/info-get-route'));

// Handle 404 
app.use((req, res) => {
  logger.error('404: Page not Found: %s',req.originalUrl);
  res.status(404).send('404: Page not Found');
});

// Handle 500
app.use((error, req, res) => {
  logger.error('Internal Server Error: %s', error);
  res.status(500).send('500: Internal Server Error');
});

module.exports = app;
