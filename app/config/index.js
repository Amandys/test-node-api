const env = process.env.NODE_ENV || 'local';

const logger = require('../loggers/logger.js');
const cfg = require('./config.' + env);

logger.info('Using environment: %s', env);
console.log('Using environment: ' + env);
logger.info('config file contains : %j', cfg);

module.exports = cfg;

