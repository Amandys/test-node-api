const SaveInfoDTO = require("./info-dto");
const { BaseDTO, fields } = require('dtox');
describe("InfoDTO DTO", () => {
       
  let InfoDTO = null;

  beforeEach( () => {    
                  
  });

  afterEach( () => {
    InfoDTO = null;
  });
     
  it("should be an instance of InfoDTO ", () => {    
    InfoDTO = new InfoDTO({
     _id: "596ecc6f8acaf23ea9ee0ddb",
     id: "111",
     __v: 0,
     name: "saacsas",
     lastName: "ssssssasa",
     email: "sasa",
     date: "2017-07-20T02:59:14.041Z"
    });
    expect(InfoDTO instanceof InfoDTO).toBe(true);    
  });

  it("should be an instance of BaseDTO ", () => {    
    InfoDTO = new InfoDTO({
     _id: "596ecc6f8acaf23ea9ee0ddb",
     id: "111",
     __v: 0,
     name: "saacsas",
     lastName: "ssssssasa",
     email: "sasa",
     date: "2017-07-20T02:59:14.041Z"
    });
    expect(InfoDTO instanceof BaseDTO).toBe(true);    
  });
  })