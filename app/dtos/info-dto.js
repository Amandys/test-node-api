const { BaseDTO, fields } = require('dtox');

const INFO_MAPPING = {
  _id: fields.string(),
  id: fields.string(),
  __v: fields.number(),
  name: fields.string(),
  lastName: fields.string(),
  email: fields.string(),
  date: fields.date(),
};
module.exports = class InfoDTO extends BaseDTO {
  constructor(data) {
    super(data, INFO_MAPPING);
  }
};
