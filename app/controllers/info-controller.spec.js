var mockery = require('mockery');
var sinon = require('sinon');

describe("InfoController module", () => {
  let InfoController = null;
  let infoController = null;
  let originalTimeout;

  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });

    mockery.registerMock('../loggers/logger.js', {
      info: (param, param2) => {},
      debug: (param) => {}}
    );

    mockery.registerMock('../../config/index', {});
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;          
  });

  afterEach(() => {
    InfoController = null;
    infoController = null;
    mockery.disable();
    mockery.deregisterAll();
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });

  it("Should be an instance of InfoController", () => {                                  
    InfoController = require('./info-controller');
    infoController = new InfoController();                  
    expect(infoController instanceof InfoController).toBeTruthy();     
  });

  describe("Info method", () => {
    it("should return a promise ", (done) => {
      // mock del servicio de mongo
      let checkObject = {used: () => {}}; 

      class InfoMongoServiceMock {
        Info(info) {
          checkObject.used();
          return new Promise((resolve, reject) => {
            resolve({status:"ok", response:"ok"});
            //reject("error");
          });
        }
      }
      
      let checkObjectSpy = sinon.spy(checkObject, 'used'); 
      mockery.registerMock('../services/persistence/info-mongo-service', InfoMongoServiceMock);
      
      // instanciar el sujeto de pruebas
      InfoController = require('./info-controller');
      infoController = new InfoController();               
      let info = 1;

      // probar el sujeto para que de OK
      InfoController.Info(info).then((response) => {
        expect(response.status).toBe("ok");
        expect(response.response).toBe("ok");
        expect(checkObjectSpy.callCount).toBe(1);
        done();
      })
      .catch((error) => {
        throw "should be in the then block";
      });
    });

    it("should return an error ", (done) => {
      // mock del servicio de mongo
      let checkObject = {used: () => {}}; 

      class InfoMongoServiceMock {
        Info(info) {
          checkObject.used();
          return new Promise((resolve, reject) => {
            reject({status:"error", response:"error"});
            // resolve({status:"ok", response:"ok"});
          });
        }
      }
      let checkObjectSpy = sinon.spy(checkObject, 'used'); 
      mockery.registerMock('../services/persistence/info-mongo-service', InfoMongoServiceMock);
      
      // instanciar el sujeto de pruebas
      InfoController = require('./info-controller');
      infoController = new InfoController();
      let info =  0;

      // probar el sujeto para que de error
      infoController.Info(info).then((response) => {
        throw "should be in the catch block";
      })
      .catch((error) => {
        expect(error.status).toBe("error");
        expect(error.response).toBe("error");
        expect(checkObjectSpy.callCount).toBe(1);
        done();
      });
    });
  });
});
