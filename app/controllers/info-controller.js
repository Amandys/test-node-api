const InfoMongoService = require('../services/persistence/info-mongo-service');

module.exports = class InfoController {
  Info(Id) {
    const infoMongoService = new InfoMongoService();
    return new Promise((resolve, reject) => {
      infoMongoService.Info(Id)
      .then((infoDTO) => {
        resolve(infoDTO);
      })
      .catch((error) => {
        reject(error);
      });
    });
  }
};
