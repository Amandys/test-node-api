const express = require('express');
const helper = require('../common/commonHelper');
const InfoController = require('../controllers/info-controller');

const router = express.Router();

function handler(request, response) {
  let toReturn;
  const InfoDTOId = request.params.Infoid;
  const infoController = new InfoController();
  infoController.Info(InfoDTOId)
  .then((result) => {
    toReturn = helper.setResponse(response, result);
  }).catch((error) => {
    toReturn = helper.setResponse(response, error);
  });
  return toReturn;
}
router.get('/info/:Infoid', handler);
module.exports = router;
module.exports.handler = handler;
