const mockery = require('mockery');
const sinon = require('sinon');
const _ = require("lodash");         
describe('InfoMongoService module', () => {    
    let originalTimeout;
    beforeEach( () => {
        mockery.enable({
            warnOnReplace: false,
            warnOnUnregistered: false,
            useCleanCache: true
        });
        mockery.registerMock('../../loggers/logger.js', {error: (info) => {}});
        mockery.registerMock('../../common/commonHelper.js', {});
        mockery.registerMock('../../config/index', {mongoConfig:{ "mongoTimeout": 0}});
        mockery.registerMock('../../config/common', {});
        mockery.registerMock('./init-mongo', () => {});
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000;
    });

    afterEach( () => {
        mockery.disable();
        mockery.deregisterAll();
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    it("Should be an instance of FlowUserMongoService", () => {
        const InfoMongoService = require('./info-mongo-service');
        const infoMongoService = new InfoMongoService();
        expect(infoMongoService instanceof InfoMongoService).toBeTruthy();
    });
    
    describe("Info method",  () => {

        it("Should return a promise without any problem", (done) => {
            let flag_found_in_the_mock = false;
            Document = () => {}
            Document.findOne = (query) => {
                flag_changed_in_the_mock = true;
                return new Promise( (resolve, reject) => {
                    resolve("any doc");
                });
            };
            let documentSpy = sinon.spy(Document, 'findOne');
            mockery.registerMock('../../models/info-document', Document);            
            const InfoMongoService = require('./info-mongo-service');
            const infoMongoService = new InfoMongoService();                    
            infoMongoService.Info({}).then( (doc) => {
                expect(doc).toBe("any doc");
                expect(flag_found_in_the_mock).toBeTruthy();
                expect(documentSpy.callCount).toBe(1);
                done();
            }).catch(function (err) {
                throw Error("should be in then block " + err);
            });
        });

        it("Should handle an error", (done) => {
            let flag_found_in_the_mock = false;
            Document = () => {}
            Document.findOne = (query) => {
                flag_found_in_the_mock = true;
                return new Promise( (resolve, reject) => {
                    reject("error");
                });
            };
            let documentSpy = sinon.spy(Document, 'findOne');
            mockery.registerMock('../../models/info-document', Document);            
            const InfoMongoService = require('./info-mongo-service');
            const infoMongoService = new InfoMongoService();                    
            infoMongoService.Info({}).then( (doc) => {
                throw Error("should be in catch block " + err);
            }).catch(function (err) {
                expect(err).toBe("error");
                expect(flag_found_in_the_mock).toBeTruthy();
                expect(documentSpy.callCount).toBe(1);
                done();                
            });
        });

    });    

    describe("saveInfo method",  () => {

        it("Should return a promise without any problem", (done) => {
            let flag_changed_in_the_mock = false;
            Document = () => {}
            Document.findOneAndUpdate = (query, model, options) => {
                flag_changed_in_the_mock = true;
                return new Promise( (resolve, reject) => {
                    resolve("any doc");
                });
            };
            let documentSpy = sinon.spy(Document, 'findOneAndUpdate');
            mockery.registerMock('../../models/info-document', Document);            
            const InfoMongoService = require('./info-mongo-service');
            const infoMongoService = new InfoMongoService();                    
            infoMongoService.saveInfo({}).then( (doc) => {
                expect(doc).toBe("any doc");
                expect(flag_changed_in_the_mock).toBeTruthy();
                expect(documentSpy.callCount).toBe(1);
                done();
            }).catch(function (err) {
                throw Error("should be in then block " + err);
            });
        });

        it("Should handle an error", (done) => {
            let flag_changed_in_the_mock = false;
            Document = () => {}
            Document.findOneAndUpdate = (query, model, options) => {
                flag_changed_in_the_mock = true;
                return new Promise( (resolve, reject) => {
                    reject("error");
                });
            };
            let documentSpy = sinon.spy(Document, 'findOneAndUpdate');
            mockery.registerMock('../../models/info-document', Document);            
            const InfoMongoService = require('./info-mongo-service');
            const infoMongoService = new InfoMongoService();                    
            infoMongoService.saveInfo({}).then( (doc) => {
                throw Error("should be in catch block " + err);
            }).catch(function (err) {
                expect(err).toBe("error");
                expect(flag_changed_in_the_mock).toBeTruthy();
                expect(documentSpy.callCount).toBe(1);
                done();                
            });
        });

    });

    describe("createOrUpdateDocument method",  () => {

        it("Should update a row",  (done) => {
            let flag_changed_in_the_mock = false;
            Document = () => {}
            Document.findOneAndUpdate = (query, model, options) => {
                flag_changed_in_the_mock = true;
                return new Promise( (resolve, reject) => {
                    resolve("any doc");
                });
            };
            let documentSpy = sinon.spy(Document, 'findOneAndUpdate');
            mockery.registerMock('../../models/info-document', Document);            
            const InfoMongoService = require('./info-mongo-service');
            const infoMongoService = new InfoMongoService();        
            const update={sessionId:1};
            infoMongoService.createOrUpdateDocument({},update).then( (doc) => {
                expect(doc).toBe("any doc");
                expect(flag_changed_in_the_mock).toBeTruthy();
                expect(documentSpy.callCount).toBe(1);
                done();
            }).catch( (err) => {
                throw Error("should be in then block " + err);
            });
        });

        it("Should handle an error", (done) => {
            let flag_changed_in_the_mock = false;
            Document = () => {}
            Document.findOneAndUpdate = (query, model, options) => {
                flag_changed_in_the_mock = true;
                return new Promise( (resolve, reject) => {
                    reject("error");
                });
            };
            let documentSpy = sinon.spy(Document, 'findOneAndUpdate');
            mockery.registerMock('../../models/info-document', Document);            
            const InfoMongoService = require('./info-mongo-service');
            const infoMongoService = new InfoMongoService();        
            const update={sessionId:1};
            infoMongoService.createOrUpdateDocument({},update).then( (doc) => {
                throw Error("should be in catch block " + err);                
            }).catch( (err) => {
                expect(err).toBe("error");
                expect(flag_changed_in_the_mock).toBeTruthy();
                expect(documentSpy.callCount).toBe(1);
                done();
            });
        });

    });

    describe("setUpdateDate method", () => {
        
        it("should return an object with one extra attribute called updatedDate", () => {
            let InfoMongoService = require('./info-mongo-service');
            // to test date
            InfoMongoService.prototype.getDate = () => {return "value"};
            const infoMongoService = new InfoMongoService();
            const object = infoMongoService.setDate({});                                   
            expect(object.date).toEqual("value");
        });

    });
      
    describe("getQueryBy method", () => {
        
        it("should return an object with one attribute", () => {
            const InfoMongoService = require('./info-mongo-service');
            const infoMongoService = new InfoMongoService();  
            const object = infoMongoService.getQueryBy("attribute","value");                    
            expect(object.attribute).toEqual("value");
        });

    });

});