const logger = require('../../loggers/logger');
const infoDocument = require('../../models/info-document');
const config = require('../../config/index');

// connect to mongodb.
const initMongo = require('./init-mongo');

module.exports = class InfoMongoService {

  constructor() {
    initMongo();
  }

  Info(id) {
    return new Promise((resolve, reject) => {
      const query1 = this.getQueryBy('id', id);
      infoDocument.findOne(query1)
      .then((doc) => {
        logger.info('Usuario: %s', JSON.stringify(doc));
        resolve(doc);
      })
      .catch((err) => {
        logger.error(`error in mongo, ${err}!`);
        reject(err);
      });
      setTimeout(() => {
        reject('Mongo connection error');
      }, config.mongoConfig.mongoTimeout);
    });
  }

  saveInfo(document) {
    const doc = this.setDate(document);
    const query = this.getQueryBy('id', doc.id);
    return this.createOrUpdateDocument(doc, query);
  }

  createOrUpdateDocument(document, query) {
    return new Promise((resolve, reject) => {
      infoDocument.findOneAndUpdate(query, document,
      { upsert: true, setDefaultsOnInsert: true, new: true })
      .then((doc) => {
        logger.info('user flow saved: %s', JSON.stringify(document));
        resolve(doc);
      })
      .catch((err) => {
        logger.error(`error in mongo, ${err}!`);
        reject(err);
      });
      setTimeout(() => {
        reject('Mongo connection error');
      }, config.mongoConfig.mongoTimeout);
    });
  }

  getQueryBy(field, value) {
    return { [field]: value };
  }

  setDate(document) {
    const doc = document;
    doc.date = this.getDate();
    return doc;
  }

  getDate() {
    return new Date();
  }

};

